## Plotting script

# Setup Instructions on lxplus access machines:

```
source setupRoot.sh
```

# Structure of the script:

The main script that has all the functions is ```makeplot.py```. You can write your own script which uses these functions to plot any kind of overlay histogram. Examples of these scripts are basically given as ```ex_*.py``` files.

```Overlay``` is the class where you can get an overlay of histograms. You can define histograms with function ```AddHistogram()```. An array ```_hists``` is defined and histograms are assigned from index 1. ```MakeOverlay()``` is the function which plots all the histograms in an overlay. You can define the style of the histogram using the function ```AddHistStyle```. Other functions are defined and described inside the script.

