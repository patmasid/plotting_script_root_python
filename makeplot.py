#! /usr/bin/env python
import ROOT
import os,string,random
import argparse,collections
from array import array
import sys
import math
import numpy as np

ROOT.gROOT.SetBatch()

class Overlay():
    
    def __init__(self, output_dir, imagename, Ytitle, Yratiotitle, imagetype):
        self._canvas = ROOT.TCanvas("canvas","canvas",800,800)
        self._output_dir = output_dir
        self._imagename = imagename
        self._Ytitle = Ytitle
        self._Yratiotitle = Yratiotitle
        self._imagetype = imagetype
        self._Npads = 2
        self._Nhists = 0
        self._Nhistpairs = 0
        self._Nbins = 0
        self._isratio = True
        self._isshapecomp = False
        self._islogy = False
        self._histmax = 0.0
        self._legend = ROOT.TLegend(0.45,0.75,0.9,0.9)
        self._Nlegcols = 2
        self._RatioWithHistN = 1
        
        self._hists = [None] * 25
        self._profiles = [None] * 25
        self._errhists = [None] * 25
        self._lineerrhists = [None] * 25
        self._histnames = [None] * 25
        self._axisnames = [None] * 25
        self._rootfiles = [None] * 25
        self._ratiohists = [None] * 25
        self._ratioerrhists = [None] * 25
        self._legnames = [None] * 25
        self._hist_scales = [None] * 25
        self._pads = [None] * 25
        self._histpairs = [None] * 10
        

        self._colors = collections.OrderedDict()
        self._colors[1] = ROOT.kBlack
        self._colors[2] = ROOT.kBlue
        self._colors[3] = ROOT.kRed
        self._colors[4] = ROOT.kGreen+1
        self._colors[5] = ROOT.kMagenta
        self._colors[6] = ROOT.kCyan
        self._colors[7] = ROOT.kOrange-3
        self._colors[8] = ROOT.kBlack
        self._colors[9] = ROOT.kBlue
        self._colors[10] = ROOT.kRed
        self._colors[11] = ROOT.kGreen+1
        self._colors[12] = ROOT.kMagenta
        self._colors[13] = ROOT.kOrange-3
        self._colors[14] = ROOT.kCyan
        
        self._linestyles = collections.OrderedDict()
        self._linestyles[1] = 1
        self._linestyles[2] = 1
        self._linestyles[3] = 1
        self._linestyles[4] = 1
        self._linestyles[5] = 1
        self._linestyles[6] = 1
        self._linestyles[7] = 1
        self._linestyles[8] = 7
        self._linestyles[9] = 7
        self._linestyles[10] = 7
        self._linestyles[11] = 7
        self._linestyles[12] = 7
        self._linestyles[13] = 7
        self._linestyles[14] = 7
        
        self._paircolors = collections.OrderedDict()
        self._paircolors[1] = ROOT.kBlack
        self._paircolors[2] = ROOT.kBlue
        self._paircolors[3] = ROOT.kRed
        self._paircolors[4] = ROOT.kGreen+1
        self._paircolors[5] = ROOT.kMagenta
        self._paircolors[6] = ROOT.kCyan
        self._paircolors[7] = ROOT.kOrange-3

        self._stylepairs = (1,7)

    def AddHistogram(self, N, name, axis, f_path, leg_name, scale):
        
        self._Nhists = N
        print("f_path:",f_path)
        self._rootfiles[N] = ROOT.TFile(f_path, "READ")
        self._hists[N] = self._rootfiles[N].Get(name)
        if(self._isshapecomp):
            self._hists[N].Scale(1/self._hists[N].Integral())
            self._Ytitle = "Normalized to Unity"
        self._histnames[N] = name
        self._axisnames[N] = axis

        nbins = 0
        if(N==1):
            self._Nbins = self._hists[N].GetNbinsX()
        else:
            nbins = self._hists[N].GetNbinsX()
            if(nbins != self._Nbins):
                raise RuntimeError(f'The number of bins do not match')

        if( not(self._hists[N].InheritsFrom("TH1")) ):
            raise RuntimeError(f'The histogram number {N} is not inherited from TH1.')

        self._legnames[N] = leg_name
        self._hist_scales[N] = scale
        
        self._hists[N].GetXaxis().SetLabelOffset(999)
        self._hists[N].GetYaxis().SetTitle(self._Ytitle)
        self._hists[N].GetYaxis().SetTitleSize(0.05)
        self._hists[N].GetYaxis().SetTitleOffset(1.0)

        self._lineerrhists[N] = self._hists[N].Clone(self._hists[N].GetName())

        self._errhists[N] = self._hists[N].Clone(self._hists[N].GetName())
        
        if(self._hists[N].GetMaximum() > self._histmax):
            self._histmax = self._hists[N].GetMaximum()

    def AddHistStyle(self):
        
        if(self._Nhistpairs==0):
            for ih in range(1,self._Nhists+1):
                self._hists[ih].SetLineColor(self._colors[ih])
                self._hists[ih].SetLineStyle(self._linestyles[ih])
                self._hists[ih].SetLineWidth(2)
                self._lineerrhists[ih].SetFillStyle(3354)
                self._lineerrhists[ih].SetFillColor(self._colors[ih])
                self._lineerrhists[ih].SetLineColor(self._colors[ih])
                self._lineerrhists[ih].SetLineWidth(2)
                self._errhists[ih].SetFillStyle(3354)
                self._errhists[ih].SetFillColor(self._colors[ih])
                self._errhists[ih].SetLineWidth(0)
        else:
            for ipair in range(1,self._Nhistpairs+1):
                N1 = self._histpairs[ipair][0]
                N2 = self._histpairs[ipair][1]
                if(N1!=N2):
                    self._hists[N1].SetLineColor(self._paircolors[ipair])
                    self._hists[N1].SetLineStyle(self._stylepairs[0])
                    self._hists[N1].SetLineWidth(2)
                    self._lineerrhists[N1].SetFillStyle(3354)
                    self._lineerrhists[N1].SetFillColor(self._paircolors[ipair])
                    self._lineerrhists[N1].SetLineColor(self._paircolors[ipair])
                    self._lineerrhists[N1].SetLineWidth(2)
                    self._errhists[N1].SetFillStyle(3354)
                    self._errhists[N1].SetFillColor(self._paircolors[ipair])
                    self._errhists[N1].SetLineWidth(0)
                    
                    self._hists[N2].SetLineColor(self._paircolors[ipair])
                    self._hists[N2].SetLineStyle(self._stylepairs[1])
                    self._hists[N2].SetLineWidth(2)
                    self._lineerrhists[N2].SetFillStyle(3354)
                    self._lineerrhists[N2].SetFillColor(self._paircolors[ipair])
                    self._lineerrhists[N2].SetLineColor(self._paircolors[ipair])
                    self._lineerrhists[N2].SetLineWidth(2)
                    self._lineerrhists[N2].SetLineStyle(self._stylepairs[1])
                    self._errhists[N2].SetFillStyle(3354)
                    self._errhists[N2].SetFillColor(self._paircolors[ipair])
                    self._errhists[N2].SetLineWidth(0)
                else:
                    self._hists[N1].SetLineColor(self._paircolors[ipair])
                    self._hists[N1].SetLineStyle(self._stylepairs[0])
                    self._hists[N1].SetLineWidth(2)
                    self._lineerrhists[N1].SetFillStyle(3354)
                    self._lineerrhists[N1].SetFillColor(self._paircolors[ipair])
                    self._lineerrhists[N1].SetLineColor(self._paircolors[ipair])
                    self._lineerrhists[N1].SetLineWidth(2)
                    self._errhists[N1].SetFillStyle(3354)
                    self._errhists[N1].SetFillColor(self._paircolors[ipair])
                    self._errhists[N1].SetLineWidth(0)

    def AddHistColCouple(self, n, N1, N2):
        self._Nhistpairs = n
        self._histpairs[n] = (N1,N2)

    def GetNthColor(self, N):
        return self._colors[N]
       
    def SetNLegColumns(self, Ncols):
        self._Nlegcols = Ncols

    def SetIsShapeComp(self, isshape):
        self._isshapecomp = isshape

    def GetNthLineStyle(self, N):
        return self._linestyles[N]

    def SetNpads(self, num):
        self._Npads = num

    def SetRatioWithHistN(self, num):
        self._RatioWithHistN = num

    def MakeOverlay(self, isRatio=True, islog=False):
        ROOT.gStyle.SetOptStat(0)
        ROOT.gStyle.SetOptTitle(0)
        self.AddHistStyle()
        self._canvas.cd()
        self._islogy = islog
        if(self._islogy):
            self._canvas.SetLogy()
        
        if(isRatio):
            self._pads[1] = ROOT.TPad("pad1","pad1",0, 0.27, 1.0, 1.0)
            self._pads[1].SetTopMargin(0.05)
            self._pads[1].SetLeftMargin(0.12)
            self._pads[1].SetRightMargin(0.05)
            self._pads[1].SetBottomMargin(0.02)
            self._pads[1].Draw()
            self._pads[1].cd()
            self._pads[1].SetTicks(1,1)
            
            for ih in range(1,self._Nhists+1):
                if(ih == self._RatioWithHistN):
                    self._hists[ih].SetMaximum(self._histmax*1.7)
                    self._hists[ih].Draw("E2 hist")
                    self._errhists[ih].Draw("E2 same")
                else:
                    self._hists[ih].Draw("E2 hist same")
                    self._errhists[ih].Draw("E2 same")

            ROOT.gPad.Update()
            ROOT.gPad.Modified()

            self._canvas.cd()
            self._pads[2] = ROOT.TPad("pad2","pad2",0, 0, 1, 0.27)
            self._pads[2].SetTopMargin(0.03)
            self._pads[2].SetLeftMargin(0.12)
            self._pads[2].SetRightMargin(0.05)
            self._pads[2].SetBottomMargin(0.3)
            self._pads[2].Draw()
            self._pads[2].cd()
            self._pads[2].SetTicks(1,1)
            
            for ih in range(2,self._Nhists+1):
                self._ratiohists[ih] = self._hists[ih].Clone(self._hists[ih].GetName())
                self._ratiohists[ih].Divide(self._hists[self._RatioWithHistN])
                self._ratioerrhists[ih] = self._hists[ih].Clone(self._hists[ih].GetName())
                self._ratioerrhists[ih].Divide(self._hists[self._RatioWithHistN])
                for ibin in range(1,self._Nbins):
                    if(self._hists[ih].GetBinContent(ibin)!=0):
                        self._ratiohists[ih].SetBinError(ibin, self._hists[ih].GetBinError(ibin)/self._hists[ih].GetBinContent(ibin))
                        self._ratioerrhists[ih].SetBinError(ibin, self._hists[ih].GetBinError(ibin)/self._hists[ih].GetBinContent(ibin))
                    else:
                        self._ratiohists[ih].SetBinError(ibin, 0)
                        self._ratioerrhists[ih].SetBinError(ibin, 0)
                    
                self._ratiohists[ih].SetAxisRange(0.5,2,"Y");
                self._ratiohists[ih].GetYaxis().SetLabelSize(0.1);
                self._ratiohists[ih].GetYaxis().SetTitle(self._Yratiotitle);
                self._ratiohists[ih].GetYaxis().SetTitleSize(0.12);
                self._ratiohists[ih].GetYaxis().SetTitleOffset(0.4);
                
                self._ratiohists[ih].GetXaxis().SetLabelSize(0.1);
                self._ratiohists[ih].GetXaxis().SetTitle(self._axisnames[ih]);
                self._ratiohists[ih].GetXaxis().SetTitleSize(0.12);
                self._ratiohists[ih].GetXaxis().SetTitleOffset(1);
                
                self._ratioerrhists[ih].SetFillStyle(3354)
                self._ratioerrhists[ih].SetFillColor(self._colors[ih])
                self._ratioerrhists[ih].SetLineWidth(0)

            h_base = self._hists[self._RatioWithHistN].Clone(self._hists[self._RatioWithHistN].GetName())
            h_base.Reset()

            for ibin in range(1,self._Nbins+1):
                h_base.SetBinContent(ibin,1)
                if(self._hists[self._RatioWithHistN].GetBinContent(ibin)!=0):
                    h_base.SetBinError(ibin, self._hists[self._RatioWithHistN].GetBinError(ibin)/self._hists[self._RatioWithHistN].GetBinContent(ibin))
                else:
                    h_base.SetBinError(ibin, 0);

            h_base.SetFillStyle(3354)
            h_base.SetLineColor(ROOT.kBlack)
            h_base.SetLineWidth(0)
            h_base.SetAxisRange(0.5,2,"Y")
            h_base.GetYaxis().SetLabelSize(0.1);
            h_base.GetYaxis().SetTitle(self._Yratiotitle);
            h_base.GetYaxis().SetTitleSize(0.12);
            h_base.GetYaxis().SetTitleOffset(0.4);
            h_base.GetXaxis().SetLabelSize(0.1);
            h_base.GetXaxis().SetLabelOffset(0.005)
            h_base.GetXaxis().SetTitle(self._axisnames[ih]);
            h_base.GetXaxis().SetTitleSize(0.12);
            h_base.GetXaxis().SetTitleOffset(1);
            h_base.Draw("E2")

            ROOT.gPad.RedrawAxis();

            for ih in range(2,self._Nhists+1):
                self._ratiohists[ih].Draw("E2 hist same");
                #self._ratioerrhists[ih].Draw("E2 same")
                self._ratiohists[ih].Draw("E2 hist same");
                #self._ratioerrhists[ih].Draw("E2 same");
            
            hh = h_base.DrawCopy("HIST SAME")
            hh.SetFillStyle(0)
            hh.SetLineColor(ROOT.kBlack)
            hh.SetLineWidth(2)

            ROOT.gPad.Update()
            
            self._canvas.cd()

            atlaslabel = ROOT.TLatex(0.16,0.88,"#it{ATLAS Work in Progress}")
            atlaslabel.SetTextSize(0.025)
            sqrtslabel = ROOT.TLatex(0.16,0.81 ,"#bf{#sqrt{s} = 13 TeV, 140 fb^{-1}}")
            sqrtslabel.SetTextSize(0.025)
            atlaslabel.Draw("same")
            sqrtslabel.Draw("same")
            
            ROOT.gStyle.SetLegendTextSize(0.03)
            self._legend.SetNColumns(self._Nlegcols)
            for ih in range(1,self._Nhists+1):
                self._legend.AddEntry(self._lineerrhists[ih], self._legnames[ih], "L F")
            #self._legend.AddEntry(h_base, "Tot. Uncert.", "L F");
            
            self._canvas.cd()
            self._legend.Draw("same")

            self._canvas.Print( os.path.join(self._output_dir, self._imagetype, self._imagename+"."+self._imagetype) )

    def SetIsRatio(self, isratio):
        self._isratio = isratio

    def MakeOverlay_prof(self, isRatio=True, islog=False):
        ROOT.gStyle.SetOptStat(0)
        ROOT.gStyle.SetOptTitle(0)
        self.AddHistStyle()
        self._canvas.cd()
        self._islogy = islog
        if(self._islogy):
            self._canvas.SetLogy()

        if(isRatio):
            self._pads[1] = ROOT.TPad("pad1","pad1",0, 0.27, 1.0, 1.0)
            self._pads[1].SetTopMargin(0.05)
            self._pads[1].SetLeftMargin(0.12)
            self._pads[1].SetRightMargin(0.05)
            self._pads[1].SetBottomMargin(0.02)
            if(self._islogy):
                self._pads[1].SetLogy()
            self._pads[1].Draw()
            self._pads[1].cd()
            self._pads[1].SetTicks(1,1)

            for ih in range(1, self._Nhists + 1):
                self._profiles[ih].SetLineColor(self._colors[ih])  
                self._profiles[ih].SetMarkerColor(self._colors[ih])
                self._errhists[ih].SetLineColor(self._colors[ih])  
                self._errhists[ih].SetMarkerColor(self._colors[ih])
                if ih == self._RatioWithHistN:
                    self._profiles[ih].SetMaximum(self._histmax * 1.7)
                    self._profiles[ih].Draw("P E1 hist same")  # Draw the profile with error bars
                    self._errhists[ih].Draw("P E1 same")
                else:
                    self._profiles[ih].Draw("P E1 same")  # Draw the profile with error bars
                    self._errhists[ih].Draw("P E1 same")

            ROOT.gPad.Update()
            ROOT.gPad.Modified()

            self._canvas.cd()
            self._pads[2] = ROOT.TPad("pad2","pad2",0, 0, 1, 0.27)
            self._pads[2].SetTopMargin(0.03)
            self._pads[2].SetLeftMargin(0.12)
            self._pads[2].SetRightMargin(0.05)
            self._pads[2].SetBottomMargin(0.3)
            self._pads[2].Draw()
            self._pads[2].cd()
            self._pads[2].SetTicks(1,1)

            for ih in range(2,self._Nhists+1):
                self._ratiohists[ih] = self._hists[ih].Clone(self._hists[ih].GetName())
                self._ratiohists[ih].Divide(self._hists[self._RatioWithHistN])
                self._ratioerrhists[ih] = self._hists[ih].Clone(self._hists[ih].GetName())
                self._ratioerrhists[ih].Divide(self._hists[self._RatioWithHistN])
                for ibin in range(1,self._Nbins):
                    if(self._hists[ih].GetBinContent(ibin)!=0):
                        self._ratiohists[ih].SetBinError(ibin, self._hists[ih].GetBinError(ibin)/self._hists[ih].GetBinContent(ibin))
                        self._ratioerrhists[ih].SetBinError(ibin, self._hists[ih].GetBinError(ibin)/self._hists[ih].GetBinContent(ibin))
                    else:
                        self._ratiohists[ih].SetBinError(ibin, 0)
                        self._ratioerrhists[ih].SetBinError(ibin, 0)

                self._ratiohists[ih].SetAxisRange(0.5,2,"Y");
                self._ratiohists[ih].GetYaxis().SetLabelSize(0.1);
                self._ratiohists[ih].GetYaxis().SetTitle(self._Yratiotitle);
                self._ratiohists[ih].GetYaxis().SetTitleSize(0.12);
                self._ratiohists[ih].GetYaxis().SetTitleOffset(0.4);

                self._ratiohists[ih].GetXaxis().SetLabelSize(0.1);
                self._ratiohists[ih].GetXaxis().SetTitle(self._axisnames[ih]);
                self._ratiohists[ih].GetXaxis().SetTitleSize(0.12);
                self._ratiohists[ih].GetXaxis().SetTitleOffset(1);

                self._ratioerrhists[ih].SetFillStyle(3354)
                self._ratioerrhists[ih].SetFillColor(self._colors[ih])
                self._ratioerrhists[ih].SetLineWidth(0)

            h_base = self._hists[self._RatioWithHistN].Clone(self._hists[self._RatioWithHistN].GetName())
            h_base.Reset()

            for ibin in range(1,self._Nbins+1):
                h_base.SetBinContent(ibin,1)
                if(self._hists[self._RatioWithHistN].GetBinContent(ibin)!=0):
                    h_base.SetBinError(ibin, self._hists[self._RatioWithHistN].GetBinError(ibin)/self._hists[self._RatioWithHistN].GetBinContent(ibin))
                else:
                    h_base.SetBinError(ibin, 0);

            h_base.SetFillStyle(3354)
            h_base.SetLineColor(ROOT.kBlack)
            h_base.SetLineWidth(0)
            h_base.SetAxisRange(0.5,2,"Y")
            h_base.GetYaxis().SetLabelSize(0.1);
            h_base.GetYaxis().SetTitle(self._Yratiotitle);
            h_base.GetYaxis().SetTitleSize(0.12);
            h_base.GetYaxis().SetTitleOffset(0.4);
            h_base.GetXaxis().SetLabelSize(0.1);
            h_base.GetXaxis().SetLabelOffset(0.005)
            h_base.GetXaxis().SetTitle(self._axisnames[ih]);
            h_base.GetXaxis().SetTitleSize(0.12);
            h_base.GetXaxis().SetTitleOffset(1);
            h_base.Draw("E2")

            ROOT.gPad.RedrawAxis();

            for ih in range(2,self._Nhists+1):
                self._ratiohists[ih].Draw("E2 hist same");
                #self._ratioerrhists[ih].Draw("E2 same")
                self._ratiohists[ih].Draw("E2 hist same");
                #self._ratioerrhists[ih].Draw("E2 same");

            hh = h_base.DrawCopy("HIST SAME")
            hh.SetFillStyle(0)
            hh.SetLineColor(ROOT.kBlack)
            hh.SetLineWidth(2)

            ROOT.gPad.Update()

            self._canvas.cd()

            atlaslabel = ROOT.TLatex(0.16,0.88,"#it{ATLAS Work in Progress}")
            atlaslabel.SetTextSize(0.025)
            sqrtslabel = ROOT.TLatex(0.16,0.81 ,"#bf{#sqrt{s} = 13 TeV, 140 fb^{-1}}")
            sqrtslabel.SetTextSize(0.025)
            atlaslabel.Draw("same")
            sqrtslabel.Draw("same")

            ROOT.gStyle.SetLegendTextSize(0.03)
            self._legend.SetNColumns(self._Nlegcols)
            for ih in range(1,self._Nhists+1):
                self._legend.AddEntry(self._lineerrhists[ih], self._legnames[ih], "L F")

        else:
            self._canvas.cd()
            for ih in range(1, self._Nhists + 1):
                self._profiles[ih].GetXaxis().SetTitle(self._axisnames[ih])
                self._profiles[ih].SetLineColor(self._colors[ih])
                self._profiles[ih].SetMarkerColor(self._colors[ih])
                self._errhists[ih].SetLineColor(self._colors[ih])
                self._errhists[ih].SetMarkerColor(self._colors[ih])
                if ih == self._RatioWithHistN:
                    self._profiles[ih].SetMaximum(self._histmax * 1.7)
                    self._profiles[ih].Draw("P E1 hist same")  # Draw the profile with error bars
                    self._errhists[ih].Draw("P E1 same")
                else:
                    self._profiles[ih].Draw("P E1 same")  # Draw the profile with error bars
                    self._errhists[ih].Draw("P E1 same")

            ROOT.gPad.Update()

            self._canvas.cd()
            
            #atlaslabel = ROOT.TLatex()
            #atlaslabel.SetTextSize(0.025)
            #atlaslabel.DrawLatex(0.1,1000,"#it{ATLAS Work in Progress}")
            #sqrtslabel = ROOT.TLatex(0.16,0.81 ,"#bf{#sqrt{s} = 13 TeV, 140 fb^{-1}}")
            #sqrtslabel.SetTextSize(0.025)
            #atlaslabel.Draw("same")
            #sqrtslabel.Draw("same")

            ROOT.gStyle.SetLegendTextSize(0.03)
            self._legend.SetNColumns(self._Nlegcols)
            for ih in range(1,self._Nhists+1):
                self._legend.AddEntry(self._profiles[ih], self._legnames[ih], "L E")
                
        self._canvas.cd()
        self._legend.Draw("same")
        
        self._canvas.Print( os.path.join(self._output_dir, self._imagetype, self._imagename+"."+self._imagetype) )

    def MakeOverlay_withDivs(self, isRatio=True, islog=False):
        ROOT.gStyle.SetOptStat(0)
        ROOT.gStyle.SetOptTitle(0)
        self.AddHistStyle()
        self._canvas.cd()
        self._islogy = islog
        if(self._islogy):
            self._canvas.SetLogy()

        if(isRatio):
            self._pads[1] = ROOT.TPad("pad1","pad1",0, 0.27, 1.0, 1.0)
            self._pads[1].SetTopMargin(0.05)
            self._pads[1].SetLeftMargin(0.12)
            self._pads[1].SetRightMargin(0.05)
            self._pads[1].SetBottomMargin(0.02)
            if(self._islogy):
                self._pads[1].SetLogy()
            self._pads[1].Draw()
            self._pads[1].cd()
            self._pads[1].SetTicks(1,1)

            for ih in range(2,self._Nhists+1):
                if(ih == self._RatioWithHistN):
                    self._hists[ih].SetMaximum(self._histmax*1.7)
                    self._hists[ih].Draw("E2 hist")
                    self._errhists[ih].Draw("E2 same")
                else:
                    self._hists[ih].Draw("E2 hist same")
                    self._errhists[ih].Draw("E2 same")

            ROOT.gPad.Update()
            ROOT.gPad.Modified()

            self._canvas.cd()
            self._pads[2] = ROOT.TPad("pad2","pad2",0, 0, 1, 0.27)
            self._pads[2].SetTopMargin(0.03)
            self._pads[2].SetLeftMargin(0.12)
            self._pads[2].SetRightMargin(0.05)
            self._pads[2].SetBottomMargin(0.3)
            self._pads[2].Draw()
            self._pads[2].cd()
            self._pads[2].SetTicks(1,1)

            for ih in range(3,self._Nhists+1):
                self._ratiohists[ih] = self._hists[ih].Clone(self._hists[ih].GetName())
                self._ratiohists[ih].Divide(self._hists[self._RatioWithHistN])
                self._ratioerrhists[ih] = self._hists[ih].Clone(self._hists[ih].GetName())
                self._ratioerrhists[ih].Divide(self._hists[self._RatioWithHistN])
                for ibin in range(1,self._Nbins):
                    if(self._hists[ih].GetBinContent(ibin)!=0):
                        self._ratiohists[ih].SetBinError(ibin, self._hists[ih].GetBinError(ibin)/self._hists[ih].GetBinContent(ibin))
                        self._ratioerrhists[ih].SetBinError(ibin, self._hists[ih].GetBinError(ibin)/self._hists[ih].GetBinContent(ibin))
                    else:
                        self._ratiohists[ih].SetBinError(ibin, 0)
                        self._ratioerrhists[ih].SetBinError(ibin, 0)

                self._ratiohists[ih].SetAxisRange(0.5,2,"Y");
                self._ratiohists[ih].GetYaxis().SetLabelSize(0.1);
                self._ratiohists[ih].GetYaxis().SetTitle(self._Yratiotitle);
                self._ratiohists[ih].GetYaxis().SetTitleSize(0.12);
                self._ratiohists[ih].GetYaxis().SetTitleOffset(0.4);

                self._ratiohists[ih].GetXaxis().SetLabelSize(0.1);
                self._ratiohists[ih].GetXaxis().SetTitle(self._axisnames[ih]);
                self._ratiohists[ih].GetXaxis().SetTitleSize(0.12);
                self._ratiohists[ih].GetXaxis().SetTitleOffset(1);

                self._ratioerrhists[ih].SetFillStyle(3354)
                self._ratioerrhists[ih].SetFillColor(self._colors[ih])
                self._ratioerrhists[ih].SetLineWidth(0)

            h_base = self._hists[self._RatioWithHistN].Clone(self._hists[self._RatioWithHistN].GetName())
            h_base.Reset()

            for ibin in range(1,self._Nbins+1):
                h_base.SetBinContent(ibin,1)
                if(self._hists[self._RatioWithHistN].GetBinContent(ibin)!=0):
                    h_base.SetBinError(ibin, self._hists[self._RatioWithHistN].GetBinError(ibin)/self._hists[self._RatioWithHistN].GetBinContent(ibin))
                else:
                    h_base.SetBinError(ibin, 0);

            h_base.SetFillStyle(3354)
            h_base.SetLineColor(ROOT.kBlack)
            h_base.SetLineWidth(0)
            h_base.SetAxisRange(0.5,2,"Y")
            h_base.GetYaxis().SetLabelSize(0.1);
            h_base.GetYaxis().SetTitle(self._Yratiotitle);
            h_base.GetYaxis().SetTitleSize(0.12);
            h_base.GetYaxis().SetTitleOffset(0.4);
            h_base.GetXaxis().SetLabelSize(0.1);
            h_base.GetXaxis().SetLabelOffset(0.005)
            h_base.GetXaxis().SetTitle(self._axisnames[ih]);
            h_base.GetXaxis().SetTitleSize(0.12);
            h_base.GetXaxis().SetTitleOffset(1);
            h_base.Draw("E2")


            ROOT.gPad.RedrawAxis();


            for ih in range(3,self._Nhists+1):
                self._ratiohists[ih].Draw("E2 hist same");
                #self._ratioerrhists[ih].Draw("E2 same")
                self._ratiohists[ih].Draw("E2 hist same");
                #self._ratioerrhists[ih].Draw("E2 same");

            hh = h_base.DrawCopy("HIST SAME")
            hh.SetFillStyle(0)
            hh.SetLineColor(ROOT.kBlack)
            hh.SetLineWidth(2)

            ROOT.gPad.Update()

            self._canvas.cd()

            atlaslabel = ROOT.TLatex(0.16,0.88,"#it{ATLAS Work in Progress}")
            atlaslabel.SetTextSize(0.025)
            sqrtslabel = ROOT.TLatex(0.16,0.81 ,"#bf{#sqrt{s} = 13 TeV, 140 fb^{-1}}")
            sqrtslabel.SetTextSize(0.025)
            atlaslabel.Draw("same")
            sqrtslabel.Draw("same")

            ROOT.gStyle.SetLegendTextSize(0.03)
            self._legend.SetNColumns(self._Nlegcols)
            for ih in range(2,self._Nhists+1):
                self._legend.AddEntry(self._lineerrhists[ih], self._legnames[ih], "L F")
            #self._legend.AddEntry(h_base, "Tot. Uncert.", "L F");

            self._canvas.cd()
            self._legend.Draw("same")

            self._canvas.Print( os.path.join(self._output_dir, self._imagetype, self._imagename+"."+self._imagetype) )

    def AddHistogram_prof(self, N, name, axis, f_path, leg_name, scale):
        self._Nhists = N
        print("f_path:", f_path)
        self._rootfiles[N] = ROOT.TFile(f_path, "READ")
        self._hists[N] = self._rootfiles[N].Get(name)

        nbins = 0
        if(N==1):
            self._Nbins = self._hists[N].GetNbinsX()
        else:
            nbins = self._hists[N].GetNbinsX()
            if(nbins != self._Nbins):
                raise RuntimeError(f'The number of bins do not match')
        
        self._profiles[N] = ROOT.TProfile(name+"_profile", name+"_profile", 
                                          self._hists[N].GetNbinsX(), 
                                          self._hists[N].GetXaxis().GetXmin(), 
                                          self._hists[N].GetXaxis().GetXmax(),
                                          "s")

        for i in range(1, self._hists[N].GetNbinsX() + 1):
            for j in range(1, self._hists[N].GetNbinsY() + 1):
                print(self._hists[N].GetXaxis().GetBinCenter(i), self._hists[N].GetYaxis().GetBinCenter(j), int(self._hists[N].GetBinContent(i, j)), (self._hists[N].GetBinContent(i, j)))
                idek = self._hists[N].GetYaxis().GetBinCenter(j)#*self._hists[N].GetBinContent(i, j)
                for k in range(0, int(self._hists[N].GetBinContent(i, j))):
                    self._profiles[N].Fill(self._hists[N].GetXaxis().GetBinCenter(i), idek)

        if self._isshapecomp:
            self._profiles[N].Scale(1 / self._profiles[N].Integral())
            self._Ytitle = "Normalized to Unity"
        
        self._histnames[N] = name
        self._axisnames[N] = axis
        print(axis, self._axisnames[N])
        self._legnames[N] = leg_name
        self._hist_scales[N] = scale
    
        #self._profiles[N].GetXaxis().SetLabelOffset(999)
        self._profiles[N].GetYaxis().SetTitle(self._Ytitle)
        self._profiles[N].GetYaxis().SetTitleSize(0.05)
        self._profiles[N].GetYaxis().SetTitleOffset(1.0)
        self._lineerrhists[N] = self._profiles[N].Clone(self._profiles[N].GetName())
        self._errhists[N] = self._profiles[N].Clone(self._profiles[N].GetName())
        
        if self._profiles[N].GetMaximum() > self._histmax:
            self._histmax = self._profiles[N].GetMaximum()

        
#class StackComparison( ):
